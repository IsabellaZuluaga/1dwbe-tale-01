//Cuando quiero saber si un número es par uso n1 % 2 ===0, ver explicación cuaderno
let es_par = function(n1){
    if(n1 % 2 === 0)
    {
        return true;
    } else {
        return false;
    }
}

// la forma para simplificar lo anterior es: let es_par = num1 => num1 % 2 === 0;

document.getElementById('btn').addEventListener('click', ()=>{
    const num1 = parseInt(document.getElementById('num1').value);
    const mensaje = es_par(num1) ? "Es par" : "Es impar";
    document.getElementById('resultado').innerHTML = mensaje;
})