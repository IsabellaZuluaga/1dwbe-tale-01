
// Estas son funciones básicas
function funcionPrueba (nombre, apellido){
    console.log('Hola desde una funcion' + nombre + '' + apellido);;
}

funcionPrueba("Mauricio", "Sierra");
funcionPrueba("Lionel", "Messi");
funcionPrueba("Ronaldo", "Cristiano");

//Es recomendable no usar var para declarar una variable
//Si el var está definido dentro de una función, solo funciona dentro de esta
//Para definir una variable se usa let

let texto2 = "Hola LET JS";
texto2 = "Hola LET JS!!!!";

const texto3 = "Hola desde constante";

//CONST Y LET VS VAR
//Esto no funciona porque let solo funciona dentro del bloque
//Si lo imprimo por fuera del bloque, NO IMPRIME, debe ir dentro
//Texto2 está definida afuera, y ese console si imprime, porque
//un let definido afuera si se puede usar dentro del bloque.
if(true){
    let textoIf = 'hola desde dentro del if';
    console.log(texto2);
    console.log(textoIf);
}


function dentroDeFuncion(){
    var texto4 = "Hola texto 4";
    return texto4;
}
console.log(dentroDeFuncion());

//FUNCIONES FLECHA
//La 1ra y segunda forma es lo mismo
//1ra forma 
function saludar(){
    console.log('Hola');
}
//2da forma
let saludar2 = function(){
    console.log('Hola2');
}
saludar();
saludar2();

//Esta es una función flecha, se reemplaza la palabra funtion por una flecha
//Se usa cuando solo se declara una sola vez
let saludar3 = () => {
    console.log("Hola3");
}
saludar3();

function suma(n1, n2){
    console.log(n1 + n2);
}

let suma2 = function (n1, n2){
    console.log(n1 + n2);
}

let suma3 = (n1, n2) => {
    console.log(n1 + n2);
}

suma(1,2);
suma2(1,2);
suma3(1,2);

//cuando hay una sola línea entre los corchetes, se puede reducir la función a
let suma4 = (n1, n2) => console.log(n1 + n2);
suma4(1,2);

let elDoble = (n1) => {
    return n1 * 2;
}
console.log(elDoble(5));

//como lo anterior solo tiene un solo parámetro puedo reducirlo así y remover los paréntesis.
let elDoble2 = n1 => n1*2;
console.log(elDoble(2));

