
//Programemos una clase que tenga las propiedades de:
//nombre, apellido y edad
//Con los siguientes métodos:
// fullname: debe retornar el nombre y apellido, con espacio entre ambos
//esMayor: debe retornar true o false dependiendo si la persona es mayor de edad o no

class persona {
    constructor(nombre, apellido, edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    fullName (){
        return this.nombre + '' + this.apellido;
    }
    esMayor (){
        return this.edad >= 18;
    }
}

let persona1 = new persona("Isabella", "Zuluaga", 23);
console.log(persona1.fullName());
console.log(persona1.esMayor());