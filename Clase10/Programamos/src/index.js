var moment = require ('moment'); 
console.log("Horario Local: " + moment().format());
console.log("Horario UTC: " + moment.utc().format());
const diferencia = moment().hours() - moment.utc().hour();
console.log(`${moment().hour()} - ${moment.utc().hour()} = ${diferencia}`);
const fechaInicial = moment('2060-01-01');

if (fechaInicial.isBefore('2050-01-01')) {
    console.log('La fecha inicial es anterior al año 2050');
} else {
    console.log('La fecha inicial es posterior al año 2050');
}

console.log('otro cambio');