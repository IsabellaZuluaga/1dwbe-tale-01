require ('dotenv').config();
const express = require ('express');
const app = express();

const PORT = process.env.PORT || 3001;

app.use(express.static('./src/public'));
app.use(express.json());

const lista_usuarios = ["user1", "user2", "user3"];

//Para obtener datos
app.get('/usuarios', (req,res) => {
    res.json(lista_usuarios); //responde un json 
});

//Para crear datos
app.post('/usuarios', (req, res) => {
    const { nombre } =req.body;
    lista_usuarios.push(nombre);
    res.json('Usuario creado exitosamente');
});

//Para actualizar usuarios
app.put('/usuarios', (req,res) => {
    const { index } = req.query;
    const { nombre } = req.body;
    lista_usuarios [index] = nombre;
    res.json('usuario actualizado');
});

//Para borrar 
app.delete('/usuarios', (req,res) => {
    const { index } = req.query;
    lista_usuarios.splice(index,1);
    res.json('Usuario eliminado');    
});

app.listen(3001, () => {
    console.log('Escuchando desde el puerto' + PORT);
});