const express = require ('express');
const app = express(); 
const  { imprimirRequest, imprimirRequest2, imprimirRequest3 } = require('./middleware/imprimir.middleware');

app.use(imprimirRequest); //Debo agregar esta app.use para que se ejecute primero el middleware y después el endpoint

app.get('/ejemplo', (req,res) => {
    res.json('Respuesta ejemplo desde endpoint');
    console.log('Respuesta ejemplo desde endpoint');
});

app.get('/ejemplo2', imprimirRequest2, imprimirRequest3, (req,res) => { //El middleware está justo antes de recibir el req y el res es el imprimirRequest2
    res.json('Respuesta ejemplo desde endpoint');
    console.log('Respuesta ejemplo desde endpoint');
});

app.listen(8000, () => {console.log('Escuchando en el puerto 8000') });