//El middle ware se crea en forma de función 
function imprimirRequest(req, res, next) {
    console.log("Hora desde middleware:", Date.now());
    next(); // Indica que el middleware se cumple y deja seguir para que se ejecute el endpoint
}

function imprimirRequest2(req, res, next) {
    console.log("Path desde middleware2", req.path);//el req.path es donde se está haciendo la petición del servidor
    if(req.path === '/ejemplo2'){
        next();
    }  else {
        res.status(401).json('No autorizado');
    }
}

function imprimirRequest3(req, res, next) {
    console.log("Path desde middleware3", req.path);
    res.json('No estas autorizado');  
}

module.exports = {imprimirRequest, imprimirRequest2, imprimirRequest3} // Para exportar los middleware al index