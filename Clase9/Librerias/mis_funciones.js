function hablar(texto){
    console.log(texto);
}

function hablar_bajo(texto){
    console.log(texto.tolowerCase());
}

//Esta es la forma de exportar los módulos
//También se puede agregar la palabra export antes de la function
exports.hablarLibreria = hablar;
exports.hablar_bajo = hablar_bajo;


