//INSTRUCCIONES
//1. Crear archivo js
//2. Crear un array con hobbies favoritos
//3. Muestra cada uno de los hobbies utilizando un forEach para mostrar cada elemento

const hobbies = ['Leer', 'Jugar futbol', 'Cantar', 'Correr', 'Ver peliculas'];
const fs = require('fs');

hobbies.forEach((hobbie, indice) => {
    const texto = `${indice + 1}. ${hobbie}\n`;
    console.log(texto);
    fs.appendFileSync('listaHobbies.txt', texto, function (err) { 
        if (err) console.log(err); 
              else  console.log('Saved!');
      });    
});