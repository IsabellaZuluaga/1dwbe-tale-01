const express = require("express");
const {agregarPersonas, mostrarPersonas}=require("./models/usuario.models")
const app = express();



function condicionesCorreo(req, res, next){
    const {correo}=req.body;
    const personas=mostrarPersonas();
    if(personas.find(usuario => usuario.correo === correo)){
        res.send("Ese correo ya está en uso");
    }else if (correo===""){
        res.send("El correo está vacio");
    }else if(!correo.includes("@")) {
        res.send("Falta el arroba en el correo");
    }else {
        next();
    }
}


    
app.use(express.json());

app.get('/prueba' ,(req,res)=>{
    res.json(mostrarPersonas());
});
   

app.post('/prueba', condicionesCorreo,(req,res)=>{
    const usuario=req.body;
    personas.push(usuario);

    
    res.send("Usuario añadido");

});

app.delete('/prueba', (req,res)=>{
    personas.pop();
    res.send("Ultimo usuario añadido");
});

app.put('/prueba', (req,res)=>{
    const nuevoUsuario = req.body;
    res.send(editarPersona("jairo", nuevoUsuario));
});


    
const PORT=3000;
app.listen(PORT, () => {
    console.log("Escuchando en el puerto 3000");
});
