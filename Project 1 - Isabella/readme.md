# INFORMACIÓN
- Autor: Isabella Zuluaga Ocampo
- Correo: isazuluocampo@gmai.com
- Repositorio GitLab: https://gitlab.com/IsabellaZuluaga/1dwbe-tale-01/-/tree/master/Project%201%20-%20Isabella 

# CÓMO USAR

Normalmente el puerto usado es el 3500. Así: http://localhost:3500

Utilice el comando 'npm install' para instalar todos los módulos
usados. Esto creará la carpeta node_modules. 

Para editar una órden en estado nuevo, se deberá enviar en el cuerpo
de la solicitud una órden con todas las actualizaciones en todos los campos
que se puedan modificar (si no se desea modificar, se debe poner la misma información).

# DOCUMENTACIÓN
- /docs: En la raíz para ver la documentación por Swagger 

# USUARIOS

Para iniciar, hay dos usuarios registrados:

1. - email: isazuluocampo@gmail.com
   - password: isabella123
   - administrador: NO

2. - email: admin@gmail.com
   - password: 123
   - administrador: SÍ
