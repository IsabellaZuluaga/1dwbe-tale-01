const express = require ("express");
const app = express();


app.use(express.json());

const validarLoginMiddleware = require('../src/middleware/validarLogin.middleware');
const loginRoute = require ('../src/routes/login.route'); 

app.use("/usuarios",require("./routes/usuarios.routes"));


app.listen(3000, ()=> {
    console.log("Escuchando desde el puerto 3000");
})
 