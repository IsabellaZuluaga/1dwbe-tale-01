const basicAuth = require ('express-basic-auth'); // Requiero la librería para validar login
const obtenerusuarios = require ('../models/usuarios.models'); //Requiero acceder a la ruta en donde están los usuarios 

const validarLogin = (username, password) => {

    const usuarioFund = obtenerusuarios().find(u => u.username === username && u.password === password);
    if (usuarioFund) return true;
    else return false; 
}

module.exports = validarLogin;