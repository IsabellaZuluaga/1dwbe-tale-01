const usuarios = [
{
    id:1,
    usuario:"Admin",
    correo:"admin@gmail.com",
    username:"admin",
    password:"12345",
    telefono:"3178055480",
    direccion:"calle 12 #48a-60",
    isAdmin:true,
},
{
    id:2,
    usuario:"Santiago",
    correo:"eldon@gmail.com",
    username:"santiago123",
    password:"1234",
    telefono:"3217780130",
    direccion:"Avenida siempre viva #23-3",
    isAdmin:false
}, 
{
    id:3,
    usuario:"Hernan",
    correo:"hernandario@gmail.com",
    username:"hernan123",
    password:"123456",
    telefono:"3164567192",
    direccion:"Calle 100b #35-26",
    isAdmin:false
}

];

const obtenerusuarios = () =>{
    return usuarios;
}

const agregarusuarios = (usuario)=>{
    usuarios.push(usuario);
}

module.exports = {obtenerusuarios,agregarusuarios};


