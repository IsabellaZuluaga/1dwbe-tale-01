const { Router } = require("express");
const express = require("express");
const routes = express.Router();
const basicAuth = require ('express-basic-auth'); //Requiero la librería que instalé en la terminal (npm install express-basic-auth)
const validarLoginMiddleware = require('../middleware/validarLogin.middleware');


const unauthorizedResponse = (req) => {
    return req.auth? "Creedenciales no autorizadas":"Credenciales no proporcionadas";

};

Router.get('/',basicAuth({authorizer: validarLoginMiddleware, unauthorizedResponse: unauthorizedResponse }), 
(req,res) => {
    res.json('login exitoso');
}); 

module.exports = router; 



module.exports = routes;