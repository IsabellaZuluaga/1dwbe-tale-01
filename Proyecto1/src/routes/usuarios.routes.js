const express = require("express");
const routes = express.Router();
const {obtenerusuarios,agregarusuarios} = require("../models/usuarios.models");

routes.get("/",(req,res)=>{
    res.json(obtenerusuarios());
});

routes.post("/",(req,res)=>{
    const {id,usuario,correo,username,password,telefono,direccion} = req.body;
    if (id && usuario && correo && username && password && telefono && direccion) {
        const usuario = agregarusuarios(req.body);
        res.json(usuario);
    }else{
        res.json("El usuario no ha sido creado");
    }
}); 

routes.put("/:id",(req,res)=>{
    const existe = obtenerusuarios().some(real=>real.id===(req.params.id));
    if(existe){
        const nuevoUsuario = req.body;
        obtenerusuarios().forEach(usuario=>{
            if(usuario.id===parseInt(req.params.id)){
                usuario.username = nuevoUsuario.username ? nuevoUsuario.username : usuario.username;
                usuario.password = nuevoUsuario.password ? nuevoUsuario.password : usuario.password;
                usuario.correo = nuevoUsuario.correo ? nuevoUsuario.correo : usuario.correo;
            res.json("Usuario actualizado");
            }
        });
    }else{
        res.status(404).json("Id no encontrado")
    }
})

module.exports = routes;
