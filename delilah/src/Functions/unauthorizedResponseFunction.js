const unauthorizedResponse = (req) => {
    return req.auth? "Creedenciales no autorizadas":"Credenciales no proporcionadas";

};

module.exports = unauthorizedResponse;
