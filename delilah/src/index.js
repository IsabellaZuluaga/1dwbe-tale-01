const express = require ('express');
const basicAuth = require ('express-basic-auth');  


const app = express ();

app.use(express.json()); 

const unauthorizedResponseFunction = require ('./Functions/unauthorizedResponseFunction');

const validarLoginMiddleware = require('./middleware/validarlogin.middleware');
const loginRoute = require('./routes/login.route'); 
const signUpRoute = require('./routes/signUp.route');
const productosRoute = require('./routes/producto.route');
const metodoPagoRoute = require('./routes/metodoPago.route');  


app.use('/login', loginRoute);
app.use('/signUp', signUpRoute);
 

app.use (basicAuth({authorizer: validarLoginMiddleware, unauthorizedResponse : unauthorizedResponseFunction}));

app.use('/producto', productosRoute);
app.use('/metodoPago', metodoPagoRoute);



app.listen (6000,() => console.log('Listening on 6000')); 