const { getUsuario } = require('../models/usuario.model');

const validateEmail = (email) => {

    const searchUser = getUsuario().find(arrayUsuario => arrayUsuario.email === email);

    return searchUser ? false : true;

};

const validateInputs = (inputs) => {
    console.log(typeof inputs)
    for (const property in inputs) {
        if (typeof inputs[property] !== 'string') {
            return false;
        }
    }

    return true;
}

const validateSignUp = (req, res, next) => {

    const usuario  = req.body;
    let validInputs = false;
    if (usuario) {
        const { nombre, celular, email, direccion, username, password } = usuario;
        if (validateInputs([nombre, username, email, direccion, password, celular])) {

            if (username.trim () && nombre.trim() && celular.trim() && email.trim() && direccion.trim() && password.trim()) {
                validInputs = true;
                validateEmail(email) ? next() : res.status(400).json("Dirección de correo en uso");
            }
        }
    }

    if(!validInputs){
        res.status(400).json('Entradas inválidas');
    }

};

module.exports = validateSignUp;
