const basicAuth = require('express-basic-auth'); //Requiero la librería para el login
const { getUsuario } = require('../models/usuario.model');//Requiero la ruta en donde se encuentran los usuarios


//Creo la función del middleware
const validarLogin = (username, password) => {

    const userFound = getUsuario().find(arrayUsuario => basicAuth.safeCompare(arrayUsuario.username,username) &&
    basicAuth.safeCompare(arrayUsuario.password, password));

    return userFound? true : false;
}

module.exports = validarLogin;
