const router = require("../routes/login.route");

let id = 3;

const metodosPago = [
    {
        'id': 1,
        'nombre': 'efectivo'
    },
    {
        'id': 2,
        'nombre': 'debito',
    }, 
]

const getMetodoPago = () => {
    return metodosPago; 
}; 

const addMetodoPago = (metodoPago) => {
    metodoPago.id = id;
    id++;
    metodosPago.push (metodoPago);
};


const metodosPagoById = (id) => {
    return metodosPago.find(metodoPago => metodoPago.id == id);
}; 

const editarMetodoPago = (id, nombre) => {

    const metodoPago = metodosPagoById(id);

    if(metodoPago){
        metodoPago.nombre = nombre;
    }
}; 

const borrarMetodoPagoById = (id) => {

    const metodoPago = metodosPagoById(id);

    if(metodoPago){
        metodoPagao.splice(metodoPago.indexOf(metodoPago),1);
    }
};


module.exports = {getMetodoPago, addMetodoPago, metodosPagoById, editarMetodoPago, borrarMetodoPagoById}; 




