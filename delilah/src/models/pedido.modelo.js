const { productoById } = require ('../models/producto.model');
const { getUserByEmail } = require ('../models/usuario.model');
const { metodosPagoById } = require ('../models/metodospago.model');

const pedidos = [];

const agregarPedido = (nuevaOrden) => {

    const user = getUserByEmail (nuevaOrden.userEmail);
    const metodoPago = metodosPagoById (nuevaOrden.metodoPago);
    const date = new Date ();
    if (user && metodoPago) {

        const completarPedido = {
            'id': date.getTime(),
            'estado': 'Nuevo',
            'total': 0,
            'direccion': nuevaOrden.direccion,
            'metodoPago' : metodoPago.nombre,
            'productos': [],
            'usuario': {

                'nombre': usuario.nombre,
                'email': usuario.email,
                'celular': usuario.celular,
                'userName': usuario.userName
            }
        };

        setProductsToOrder (completarPedido, nuevaOrden);
        pedidos.push(completarPedido);
    } 
}

const setProductsToOrder = (pedido, littleProductos) => {
    
    if (littleProductos && pedido) {
        pedido.productos.length = 0;
        pedido.total = 0;
        littleProductos.forEach(littleProducto => {

            const fullproducto = productoById(littleProducto.id);

            if (fullProducto) {

                const nuevoPedidoProducto = {
                    'nombre': fullProducto.nombre,
                    'precio' : fullProducto.precio,
                    'monto': littleProducto.monto
                }
                orden.total += fullproducto.precio * littleProducto.monto;

                orden.producto.push (nuevoPedidoProducto); 
        
            }
            
        })
    }
}


const obtenerOrdenById = (id) => {
    return pedidos.find(ArrayPedido => ArrayPedido.id === id);
}

const actualizarEstado = (idPedido, estado) => {

    const pedido = obtenerOrdenById (idPedido);
    if (pedido) {
        pedido.estado = estado;
    }
};



