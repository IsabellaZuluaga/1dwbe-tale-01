let id = 6;

const productos = [
    {
        'id': 1,
        'nombre': 'Pasta Napolitana',
        'precio' : 15000 
    },
    {
        'id': 2,
        'nombre': 'Pasta al pesto',
        'precio' : 16500 
    }, 
    {
        'id': 3,
        'nombre': 'Pasta carbonara',
        'precio' : 13000 
    },
    {
        'id': 4,
        'nombre': 'Pasta tres quesos',
        'precio' : 18000 
    },
    {
        'id': 5,
        'nombre': 'Pasta veggie',
        'precio' : 20000 
    }
]; 

const getProductos = () => {
    return productos;
};

const addProductos = (producto) => {
    producto.id = id;
    id++;
    productos.push (producto);
}

const productoById = (id) => {
    return productos.find(producto => producto.id == id);
}; 

const editarProducto = (id, nombre, precio) => {

    const producto = productoById(id);

    if(producto){
        producto.nombre = nombre;
        producto. precio = precio;
    }
}; 

const borrarProductoById = (id) => {

    const producto = productoById(id);

    if(producto){
        productos.splice(productos.indexOf(producto),1);
    }
};

module.exports = {getProductos, addProductos, productoById, editarProducto,borrarProductoById}; 