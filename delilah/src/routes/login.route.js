//En todas las rutas primero requiero las librerias en este caso express y express-basic-auth
//Creo la función de la ruta express.router
const express = require('express');
const router = express.Router();
const basicAuth = require('express-basic-auth');
const validarLoginMiddleware = require('../middleware/validarlogin.middleware');
const unauthorizedResponseFunction = require("../Functions/unauthorizedResponseFunction");


router.get('/',basicAuth({authorizer: validarLoginMiddleware, unauthorizedResponse : unauthorizedResponseFunction}),
(req,res)=>{
    res.json('Login exitoso');  
});

module.exports = router;
