const express = require ('express'); 
const router = express.Router ();
const basicAuth = require ('express-basic-auth');
const unauthorizedResponseFunction = require('../Functions/unauthorizedResponseFunction');

const { getMetodoPago, addMetodoPago, metodoPagoById, editarMetodoPago, borrarMetodoPagoById } = require ('../models/metodospago.model'); 
const { addProductos } = require('../models/producto.model');

router.get ('/', (req,res)=>{
    res.json (getMetodoPago()); 
});

router.post('/', (req,res) =>{
    const metodoPago = req.body;

    const nuevoMetodoPago = {
        'nombre' : metodoPago.nombre,
   }
addMetodoPago(nuevoMetodoPago); 
res.json('Método de pago agregado');
});

router.put('/', (req,res)=>{
    const metodoPagoActualizado = req.body;

    const metodoPagoBuscado = metodosPagoById (req.params.id); 

    if(metodoBuscado){
        editarMetodoPago(req.params.id, metodoPagoActualizado.nombre);
        res.json('Metodo de pago actualizado'); 
    } else {
        res.json('El metodo de pago no fue actualizado');
    }
}); 

module.exports = router; 