const express = require('express');
const router = express.Router();
const { getProductos, addProductos, productoById, editarProducto, borrarProductoById } = require ('../models/producto.model'); 

router.get('/', (req,res) =>{
    res.json(getProductos());
}); 


//Solo los administradores pueden acceder a las rutas de aquí en adelante (middleware)
router.post('/', (req,res) => {
    const  producto  = req.body;

    const nuevoProducto = {
        'nombre' : producto.nombre,
        'precio' : producto.precio
    } 
    addProductos (nuevoProducto);
    res.json('Producto agregado');       
});

router.put('/:id',(req,res) => {
    const productoActualizado = req.body;
    
    const productoBuscado = productoById (req.params.id);

    if(productoBuscado) { 
        editarProducto (req.params.id, productoActualizado.nombre, productoActualizado.precio);
        res.json ('Producto actualizado'); 
    } else {
        res.json ('No fue actualizado'); 
    }

})

router.delete('/:id', (req,res)=> {
    const productoBuscado = productoById (req.params.id);

    if(productoBuscado) {
        borrarProductoById (req.params.id);
        res.json('Producto eliminado');
    } else {
        res.json ('No fue borrado');
    }
})

module.exports = router;


