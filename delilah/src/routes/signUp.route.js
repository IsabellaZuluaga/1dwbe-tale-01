const express = require('express');
const router = express.Router();
const {addUsuario,getUsuario} = require('../models/usuario.model');
const validarSignUpMiddleware = require('../middleware/validarSignUp.middleware');

router.post('/', validarSignUpMiddleware, (req, res) => {

    const usuario = req.body;
    const { nombre, email, celular, direccion, username, password } = usuario;

    const nuevoUsuario = 
    {
        'nombre': nombre,
        'email': email,
        'celular': celular,
        'direccion': direccion,
        'username': username,
        'password': password,
        'isAdmin' : false
    }

    addUsuario(nuevoUsuario);
    res.status(201).json("Usuario agregado");

});

router.get('/',(req,res)=>{
    res.json(getUsuario());
})

module.exports = router;
