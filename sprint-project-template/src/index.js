const express = require('express');
const app = express ();
const basicAuth =require('express-basic-auth');

app.use(basicAuth({
    users: {'admin': 'supersecret'}
}))

const usuarioRoutes = require('./routes/usuario.route'); // Importar el archivo de ruta

app.use('/usuarios', usuarioRoutes); //Toda petición que hagan a usuarios lo va a atender este archivo



app.listen(4000, () => {console.log('Escuchando del puerto 4000')});
