const usuarios = [
    {
        username: "Isabella",
        password: "12345",
        isAdmi: true
    },
    {
        username: "admin",
        password: "1234",
        isAdmi: true
    }
];

const obtenerUsuarios = () => {
    return usuarios;
}

const agregarUsuario = (usuarioNuevo) => {
    usuarios.push(usuarioNuevo);
}

module.exports = { obtenerUsuarios, agregarUsuario}; 